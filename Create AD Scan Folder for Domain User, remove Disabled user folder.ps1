﻿# Installs permission module (Carbon), if not already installed on system

<#
    Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
    choco install Carbon
    Import-Module 'Carbon'
#>



# Creates a scan folder for Enabled users, bypasses user folders already created

$Users = (get-aduser -filter {Enabled -eq $true}).samaccountname

ForEach($user in $users){

    $UserFolder = "F:\Scans\$user"

    If(Test-path "F:\scans\$user"){
        
        Write-Host "User $user Already Exists. Moving on..."

    } else {

        New-Item -ItemType Directory $UserFolder
        Grant-Permission -Identity $user -Permission FullControl -Path $UserFolder
        Grant-Permission -Identity "ccscp\domain admins" -Permission FullControl -Path $UserFolder
        Grant-Permission -Identity "administrators" -Permission FullControl -Path $UserFolder
        Sleep 1
        Revoke-Permission -identity "ccscp\users" -Path $UserFolder
        Revoke-Permission -identity "everyone" -Path $UserFolder
    }
}


# Deletes scan folders of non-active users

$DisabledUsers = (get-aduser -filter {Enabled -ne $true}).samaccountname 

ForEach($user in $DisabledUsers){

    If(Test-Path "F:\scans\$user"){

        Remove-Item -Recurse F:\scans\$user
        Write-Host "The scan folder for $user has been removed"

    } else {

        Write-Host "No scan folder for $user exists" -fore Yellow

    }
}

####################################### Still need to figure out a way to remove the folder when user does not exist. 
####################################### When user is disabled this works, but when user does not exist, it has nopthing to compare it to. 