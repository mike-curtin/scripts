﻿# Displays all Enabled computers in AD, and lists Windows Updates installed. Sorted by Install Date. Prompts for KB. type "*" for all KBs.

$EnabledComputers = (get-adcomputer -filter {Enabled -eq $true} | where {$_.distinguishedname -like "***"}).name

$EnabledComputers

$today = (Get-Date).ToString('MM/dd/yyyy')
Write-Host "Please specify KB. Use * for all KBs:" -Fore Yellow
$HotFixKB = (Read-Host) 

ForEach($comp in $EnabledComputers) {

    Write-Host "$comp -" -Fore Green
    Get-HotFix -ComputerName $comp |

    Where {
        $_.InstalledOn -gt "1/1/1601" -AND $_.InstalledOn -lt "$today" -and $_.HotFixID -like "$HotFixKB" 
    }  | sort InstalledOn

    Write-Host "===================="

}

  