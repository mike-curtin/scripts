﻿# Removes users from Group "Domain Admins" group, adds them to Group "Lockdown_Group"

Invoke-Command -computer SERVER01 -scriptblock {
$users = @(
"GH1510"
"GH1545"
"GH1638"
"GH1640"
"GH1701"
"GH1722"
"GH1831"
"GH1960"
"GH1986"
"GH2027"
"GH2109"
"GH2223"
"SA1213"
"SA1394"
"SA1426"
)

ForEach ($user in $users) {

    Add-ADGroupMember -Identity "Lockdown_Group" -members $user
    Remove-ADGroupMember -Identity "Domain Admins" -members $user -confirm:$false
    
}

Get-ADGroupMember -Identity lockdown_group | Format-Wide -Property name