﻿### Pings given IP address -> Email start of ping.
### When ping fails, emails notification.
### When ping back online, emails notification.
### Restarts loop.

# The following 2 lines ($IPAddress) and ($LocationTitle) need to be manually edited.
$IPAddress = "10.15.10.1"
$LocationTitle = "GH1510 Router" #"Computer/device description"

$MyEmail = Read-Host -Prompt "Gmail email Address?"
$Password = Read-Host -Prompt "Password for Gmail?"
Clear-Host

#Send an email. Shows notification system is online and ping has begun.
$EmailTo = $MyEmail
$EmailFrom = $MyEmail
$Subject = "Notification system online - $LocationTitle @ $IPAddress" 
$Body = "Notification system online. Starting ping of $LocationTitle @ $IPAddress" 
$SMTPServer = "smtp.gmail.com" 
$SMTPMessage = New-Object System.Net.Mail.MailMessage($EmailFrom,$EmailTo,$Subject,$Body)
$SMTPClient = New-Object Net.Mail.SmtpClient($SmtpServer, 587) 
$SMTPClient.EnableSsl = $true 
$SMTPClient.Credentials = New-Object System.Net.NetworkCredential("$MyEmail", "$Password"); 
$SMTPClient.Send($SMTPMessage)

Write-Host "Starting ping of $LocationTitle @ $IPAddress" -Fore Green

# Start of monitoring loop
Do {

    # Starts Test-NetConnection, looking for failed pings
    Do {

        $PingComputer1 = (Test-NetConnection $IPAddress).PingSucceeded
        $PingComputer2 = (Test-NetConnection $IPAddress).PingSucceeded

        $PingComputer1
        $PingComputer2

        Sleep 10
    }

    # When ping fails, it will send a notification email.
    Until (($PingComputer1 -eq $false) -and ($PingComputer2 -eq $false))

    # Send email notification that connection has been lost
    $Time = Get-Date
    $Message = Write-Host "CONNECTION HAS BEEN LOST AT $TIME! " -Fore Red

    $EmailTo = $MyEmail
    $EmailFrom = $MyEmail
    $Subject = "CONNECTION HAS BEEN LOST from $LocationTitle @ $IPAddress" 
    $Body = $Message 
    $SMTPServer = "smtp.gmail.com" 
    $SMTPMessage = New-Object System.Net.Mail.MailMessage($EmailFrom,$EmailTo,$Subject,$Body)
    $SMTPClient = New-Object Net.Mail.SmtpClient($SmtpServer, 587) 
    $SMTPClient.EnableSsl = $true 
    $SMTPClient.Credentials = New-Object System.Net.NetworkCredential("$MyEmail", "$Password"); 
    $SMTPClient.Send($SMTPMessage)

    Do {
        $PingComputer1 = (Test-NetConnection $IPAddress).PingSucceeded
        $PingComputer2 = (Test-NetConnection $IPAddress).PingSucceeded

        $PingComputer1
        $PingComputer2

        Sleep 10
    }

    # When ping fails, it will send a notification email.
    Until (($PingComputer1 -eq $true) -and ($PingComputer2 -eq $true))

    # Send email notification that connection is back online
    $Time = Get-Date
    $Message = Write-Host "CONNECTION RE-ESTABLISHED $TIME! " -Fore Green

    $EmailTo = $MyEmail
    $EmailFrom = $MyEmail
    $Subject = "CONNECTION HAS BEEN RE-ESTABLISHED from $LocationTitle @ $IPAddress" 
    $Body = $Message 
    $SMTPServer = "smtp.gmail.com" 
    $SMTPMessage = New-Object System.Net.Mail.MailMessage($EmailFrom,$EmailTo,$Subject,$Body)
    $SMTPClient = New-Object Net.Mail.SmtpClient($SmtpServer, 587) 
    $SMTPClient.EnableSsl = $true 
    $SMTPClient.Credentials = New-Object System.Net.NetworkCredential("$MyEmail", "$Password"); 
    $SMTPClient.Send($SMTPMessage)
}

Until (1 -eq 0) # end of monitoring loop

Remove-Variable Password

#Reference: https://stackoverflow.com/questions/12460950/how-to-pass-credentials-to-the-send-mailmessage-command-for-sending-emails