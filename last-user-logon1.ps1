$comp=[adsi]"WinNT://$($env:ComputerName)"
$users = $comp.Children | ?{ $_.SchemaClassName -eq 'User' }
$users | select @{L="Name";E={$_.psbase.Properties.Name.Value}},@{L="LastLogin";E={$_.psbase.Properties.LastLogin.Value}}