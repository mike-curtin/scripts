    $computersgroup = ((Get-ADComputer -Filter {(Enabled -eq $true)} | ? {$_.distinguishedname -like "*OU=**"}).name)
    $i = 0
    $j = 0
    
    Write-Host ""
    Write-Host "There are"$computersgroup.count"computers in total" -Fore Green
    Write-Host ""
    
    ForEach($computer in $computersgroup) {
    
        $computersonline = Test-Connection -ComputerName $computer -Quiet -Count 1
        
        If ($computersonline) {
            Write-Host "There is a connection to $computer" -Fore Green
    
            Do {
            $i
            $i++
            }
            While ($i -eq $computersgroup.count)
            
        } Else {
            Write-Host "ERROR. There is no connection to $computer" -Fore Yellow
            Do {
            $j
            $j++
            }
            While ($j -eq $computersgroup.count)
        }
    }
    
    Write-Host $i "computers online" -Fore Green
    Write-Host $j "computers offline" -Fore Yellow