﻿# Deletion script deletes Pictures, Downloads and Documents folder.
# Removes the task DD (deletion script). Creates the content inside of the PS1 script. Sets that script to run in Task Scheduler.
# *** Must run as logged in domain user, NOT admin!

Unregister-ScheduledTask "DD"

Remove-Item "C:\IT\" -Recurse
New-Item "C:\IT\" -type directory
New-Item "C:\IT\dd.ps1" -type file

echo "Remove-Item -recurse 'C:\users\$env:username\downloads\*'" | out-file "C:\it\dd.ps1"
echo "Remove-Item -recurse 'C:\users\$env:username\documents\*'" | out-file -append "C:\it\dd.ps1"
echo "Get-ChildItem -Path 'C:\users\$env:username\pictures\*' -Recurse -force | Where-Object { -not (`$_.psiscontainer) } | Remove-Item -Force" | out-file -append "C:\it\dd.ps1"

echo "exit" | out-file -append "C:\it\dd.ps1"

$action = new-ScheduledTaskAction -execute powershell.exe -argument '-command "C:\it\dd.ps1"'
$date = date
$duration = (New-TimeSpan -days 9999)
$interval = (New-TimeSpan -Hours 1)
$trigger = New-ScheduledTaskTrigger -Once -At $date -repetitionduration $duration -RepetitionInterval $interval
Register-ScheduledTask -Action $action -Trigger $trigger -TaskName "DD" -User "$env:computername\Admin" -Password Triangle13 -Description "Deletes files in directory Pictures, Documents, and Downloads, retains all directories"